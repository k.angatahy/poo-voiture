#include <string>

class Roue{

public:
  int taille;
  int angle_rotation;
  int limite_rotation_gauche;
  int limite_rotation_droite;
  int vitesse_de_rotation;

  
  Roue();
  ~Roue();
  
  void rouler();
  void tourner(int);
};
