#include "vitre.h"

class Portiere {

public:
  Portiere();
  Portiere(bool, std::string, std::string, int);

  ~Portiere();

  bool verrouillee;
  std::string position;
  std::string lateralisation;
  int angle;
  Vitre maVitre;

  void ouvrir(int);
  void fermer();
  void ouvrir_vitre();
  void verrouiller();
  void verrouiller(bool);
  void deverrouiller();
  bool est_verrouillee();
  bool est_ouverte();
};
