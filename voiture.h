#include <iostream>
#include "portiere.h"
#include "siege.h"
#include "roue.h"
#include <vector>

class Voiture {

public :
  
  std::vector<Portiere> portieres;
  std::vector<Siege> sieges;
  std::vector<Roue> roues;

  Voiture();
  ~Voiture();
  
void avancer();
void reculer();
void tourner();
void ouvrir_portiere(std::string _position, std::string _lateralisation);
};
