#include "portiere.h"
#include "vitre.h"
#include <iostream>

Portiere::Portiere() :
  Portiere(true, "", "", 0)
{}

Portiere::Portiere(bool _verrouillee,
                   std::string _position,
                   std::string _lateralisation,
                   int _angle) :
  verrouillee(_verrouillee),
  position(_position),
  lateralisation(_lateralisation),
  angle(_angle),
  maVitre(0, 12.345)
{}

Portiere::~Portiere(){

}

void Portiere::ouvrir(int _angle){
  if(!verrouillee){
    
    angle = _angle;
   
    if (angle == 0){
      std::cout << "Portiere fermée" << std::endl;
    }else{
      std::cout << "Angle d'ouverture changé" << std::endl;
    }
    

  }
  else{
    std::cout << "Votre porte est verrouillee" << std::endl;
  }
}

void Portiere::fermer(){
  ouvrir(0);
}

void Portiere::ouvrir_vitre(){ 
  maVitre.position_relative = 0;
}


void Portiere::verrouiller(){
  
    verrouillee = true;
    
}


void Portiere::verrouiller(bool _etat){

  if(_etat == true || _etat == false){
    verrouillee = _etat;
  }

  else{
    std::cout << "Valeur non valide" << std::endl;
  }
}


void Portiere::deverrouiller(){

  
    verrouillee = false;
    
}

bool Portiere::est_verrouillee(){
  return verrouillee;
}

bool Portiere::est_ouverte(){
  if (angle == 0){
    return false;
  }

  else {
    return true;
    }
}
