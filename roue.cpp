#include "roue.h"

Roue::Roue(){
  taille = 17; //en pouces
  angle_rotation = 0; // en degrés
  limite_rotation_gauche = -50; // en degrés
  limite_rotation_droite = 50; // en degrés
  vitesse_de_rotation = 0;
}

Roue::~Roue(){
}

void Roue::rouler(){
  vitesse_de_rotation = 100;
}

void Roue::tourner(int _vit){
  vitesse_de_rotation= _vit;
}
