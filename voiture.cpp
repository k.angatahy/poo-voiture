#include "roue.h"
#include "portiere.h"
#include "siege.h"
#include "voiture.h"
#include <iostream>
#include <string>
#include <vector>

Voiture::Voiture(){
  portieres.push_back(Portiere(true, "avant", "gauche", 0));
  portieres.push_back(Portiere(true, "avant", "droite", 0));
  portieres.push_back(Portiere(true, "arriere", "gauche", 0));
  portieres.push_back(Portiere(true, "arriere", "droite", 0));
  portieres.push_back(Portiere(true, "arriere", "milieu", 0));
  sieges.push_back(Siege());
  sieges.push_back(Siege());
  sieges.push_back(Siege());
  sieges.push_back(Siege());
  sieges.push_back(Siege());
  roues.push_back(Roue());
  roues.push_back(Roue());
  roues.push_back(Roue());
  roues.push_back(Roue());
        
};

void Voiture::avancer(){
  for(unsigned int i=0; i<= roues.size(); i++){

    roues[i].rouler();

  }
}

void Voiture::ouvrir_portiere(std::string _position, std::string _lateralisation){
  
}
