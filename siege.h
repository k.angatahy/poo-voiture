#include <iostream>
#include <string>

class Siege {

public:
  int angle_inclinaison;
  int position_horiz;
  int position_prof;

  Siege();
  ~Siege();
  
  void avancer(int);
  void reculer(int);
  void monter(int);
  void descendre(int);
  void incliner(int);
};
